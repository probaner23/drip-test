# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180721183756) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contact_profiles", force: :cascade do |t|
    t.string   "name"
    t.string   "phone_number"
    t.integer  "import_export_code_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "contact_profiles", ["import_export_code_id"], name: "index_contact_profiles_on_import_export_code_id", using: :btree

  create_table "import_export_codes", force: :cascade do |t|
    t.string   "code"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "party_details"
    t.string   "phone_number"
    t.string   "status"
    t.date     "date_of_establishment"
  end

  add_index "import_export_codes", ["code"], name: "index_import_export_codes_on_code", using: :btree

  create_table "office_addresses", force: :cascade do |t|
    t.string   "address"
    t.integer  "import_export_code_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "office_addresses", ["import_export_code_id"], name: "index_office_addresses_on_import_export_code_id", using: :btree

  add_foreign_key "contact_profiles", "import_export_codes"
  add_foreign_key "office_addresses", "import_export_codes"
end
