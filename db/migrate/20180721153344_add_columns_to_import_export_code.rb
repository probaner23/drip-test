class AddColumnsToImportExportCode < ActiveRecord::Migration
  def change
    add_column :import_export_codes, :party_details, :string
    add_column :import_export_codes, :phone_number, :string
    add_column :import_export_codes, :status, :string
    add_column :import_export_codes, :date_of_establishment, :date
  end
end
