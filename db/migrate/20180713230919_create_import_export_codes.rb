class CreateImportExportCodes < ActiveRecord::Migration
  def change
    create_table :import_export_codes do |t|
      t.string :code

      t.timestamps null: false
    end
    add_index :import_export_codes, :code
  end
end
