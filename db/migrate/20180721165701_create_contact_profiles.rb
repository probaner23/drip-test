class CreateContactProfiles < ActiveRecord::Migration
  def change
    create_table :contact_profiles do |t|
      t.string :name
      t.string :phone_number
      t.references :import_export_code, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
