class CreateOfficeAddresses < ActiveRecord::Migration
  def change
    create_table :office_addresses do |t|
      t.string :address
      t.references :import_export_code, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
