## README

Tests are available via `rake test`

Further possible enhancements:

* Currently during scraping if I do not find a table in the response page I
  return an error. This is not a fool-proof system as the underlying html design
  of the page itself might have been changed. In the future to tackle this
  problem, I would use either a mail/IM based approach which would send a
  message when the logic used to scrape starts failing consecutively over a
  period of time

