require 'test_helper'

class IecControllerTest < ActionController::TestCase
  test 'verify_success' do
    post 'verify', code: '3499000172', name: 'DHA'
    assert_response :success
  end

  test 'verify_fail' do
    post 'verify', code: '3499000171', name: 'DHA'
    expected = { status: 'FAIL', message: 'Improper code/name combo' }
    assert_equal expected, JSON.parse(@response.body, {:symbolize_names => true})
  end

  test 'fecth_fail' do
    get 'fetch', code: '3499000171'
    expected = { "status": 'FAIL', "message": 'IE code not present in datastore'}
    assert_equal expected, JSON.parse(@response.body, {:symbolize_names => true})
  end

  test 'fetch_success' do
    get 'fetch', code: '3499000172'
    assert_response :success
  end
end
