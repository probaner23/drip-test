require 'test_helper'

class ImportExportCodeTest < ActiveSupport::TestCase
  test 'Check for existence of Dharma' do
    iec_result = ImportExportCode.find_by(code: '3499000172')
    assert_equal(iec_result.nil?, false)
  end
end
