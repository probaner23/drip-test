// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require popper
//= require bootstrap
//= require_tree .

$(document).ready(function (){
    $( "#verifyButton" ).on("click", function( event ) {
        var iecName = $('#iecName').val()
        var iecCode = $('#iecCode').val()
        if (iecName == "") {
            alert("Missing mandatory value")
            event.preventDefault();
            return
        }
        if (iecCode == ""){
            alert("Missing mandatory value")
            event.preventDefault();
            return
        }
        var url = '/iec/' + iecCode + '/' + iecName
    
        $.ajax({
            type: 'POST',
            url: url,
            success: function(resp){
                console.log(resp);
                $(".modal-title").html(resp.message)
                $(".modal-body").hide()
                $('#iecName').val("")
                $('#iecCode').val("")
                $("#resultModal").modal("toggle");
                
            }
        });
    
        event.preventDefault();
    });

    $( "#fetchButton" ).on("click", function( event ) {
        var iecCode = $('#iecCodeFetch').val()
        if (iecCode == ""){
            alert("Missing mandatory value")
            event.preventDefault();
            return
        }
        var url = '/iec/' + iecCode
    
        $.ajax({
            type: 'GET',
            url: url,
            success: function(resp){
                console.log(resp);
                $(".modal-title").html(resp.message)
                if ("data" in resp){
                    $(".modal-body").show()
                    $("#iecCodeResponse").html("Code: " + resp.data.code)
                    $("#iecNameResponse").html("Name and Details: " + resp.data.party_details)
                    $("#iecPhoneNumberResponse").html("Phone Number: " + resp.data.phone_number)
                    $("#iecStatusResponse").html("Status: " + resp.data.status)
                    $("#iecDateOfEstablishmentResponse").html("Date of Establishment: " + resp.data.date_of_establishment)
                }
                $('#iecCodeFetch').val("")
                $("#resultModal").modal("toggle");
            }
        });
    
        event.preventDefault();
    });
})
