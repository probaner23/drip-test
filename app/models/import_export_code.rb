class ImportExportCode < ActiveRecord::Base
  has_many :contact_profiles
  has_many :office_addresses
end
