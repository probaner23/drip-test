class ErrorsController < ApplicationController
  protect_from_forgery unless: -> { request.format.json? }
  def not_found
    render json: { status: 'FAIL', message: 'Resource not found' }, status: :not_found
  end

  def internal_server_error
    render json: { status: 'FAIL', message: 'Something went wrong, please check back in a while' }, status: :internal_server_error
  end
end
