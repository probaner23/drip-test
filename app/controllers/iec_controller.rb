# Provides two endpoints to fetch and verify IEC data
class IecController < ApplicationController
  protect_from_forgery unless: -> { request.format.json? }
  require 'uri'
  require 'net/http'
  def verify
    if params['name'].size >= 3
      name = params['name']
    else
      render json: { status: 'FAIL', message: 'IEC name should be 3 letters or larger' }, status: :ok
      return
    end

    # check that IEC code is strictly numerical
    unless params['code'].scan(/\D/).empty?
      render json: { status: 'FAIL', message: 'IEC code should not contain alphabetical characters' }, status: :ok
      return
    end

    if params['code'].size == 10
      iec = params['code']
    else
      render json: { status: 'FAIL', message: 'IEC code should be 10 digits' }, status: :ok
      return
    end

    iec_result  = ImportExportCode.where(code: iec).first
    if iec_result
      if iec_result.party_details.upcase.include? name.upcase
        render json: { status: 'SUCCESS', message: "IEC checked and status is #{iec_result.status}" }, status: :ok
      else
        render json: { status: 'FAIL', message: 'Improper code/name combo' }, status: :ok
      end
      return
    end


    submit_params = {
      'iec' => iec,
      'name' => name
    }

    begin
      resp = Net::HTTP.post_form(URI.parse('http://dgft.delhi.nic.in:8100/dgft/IecPrint'), submit_params)
      if resp.is_a? Net::HTTPSuccess
        html = Nokogiri::HTML(resp.body)
        if html.at_css('table')
          tables = html.css('table')
          table = tables.first
          party_details = {}
          table.css('tr').css('td').each_with_index do |td, index|
            if td.text == 'Party Name and Address'
              party_details['party_details'] = table.css('tr').css('td')[index + 2].text.strip.split.join(' ')
            elsif td.text == 'Phone No'
              party_details['phone_number'] = table.css('tr').css('td')[index + 2].text.strip
            elsif td.text == 'IEC Status'
              party_details['status'] = table.css('tr').css('td')[index + 2].text.strip
            elsif td.text == 'Date of Establishment'
              party_details['date_of_establishment'] = table.css('tr').css('td')[index + 2].text.strip
            end
          end
          party_details['code'] = iec
          import_export_code = ImportExportCode.create(party_details)

          table = tables[1]
          individual_details = []
          table.css('tr').each do |tr|
            person_details = {}
            person_details['name'] = tr.css('td')[1].children[0].text.strip
            person_details['phone_number'] = tr.css('td')[1].children.last.text.split(':')[1].strip 
            individual_details.append(person_details)
          end
          import_export_code.contact_profiles.create(individual_details)

          table = tables[2]
          regional_addresses = []
          table.css('tr').each do |tr|
            office_address = tr.css('td')[1].text.split(':')[1].strip
            office_address = office_address.split.join(' ')
            regional_addresses.append('address': office_address)
          end
          import_export_code.office_addresses.create(regional_addresses)

          render json: { status: 'SUCCESS', message: "IEC checked and status is #{party_details['status']}" }, status: :ok
        else
          render json: { status: 'FAIL', message: 'Improper code/name combo' }, status: :ok
        end
      elsif resp.is_a? Net::HTTPClientError
        render json: { status: 'FAIL', message: 'Resource not found' }, status: :not_found
      end
    rescue Timeout::Error
      render json: { status: 'FAIL', message: 'Request to source website timed out' }, status: :request_timeout
    rescue SocketError
      render json: { status: 'FAIL', message: 'Could not establish connection to source website' }, status: :not_found
    rescue Errno::ECONNREFUSED
      render json: { status: 'FAIL', message: 'Request to source website could not be made could not be resolved' }, status: :not_found
    rescue StandardError => e
      # something went wrong in the code
      puts "error is #{e}"
      render json: { status: 'FAIL', message: 'Something went wrong, please check back in a while' }, status: :internal_server_error
    end
  end

  def fetch
    iec = params['code']
    iec_record = ImportExportCode.find_by(code: iec)
    if iec_record.nil?
      render json: { status: 'FAIL', message: 'IE code not present in datastore' }, status: :ok
    else
      render json: { status: 'SUCCESS', message: 'IE code found', data: {
        party_details:iec_record.party_details, code: iec_record.code,
        phone_number: iec_record.phone_number, status: iec_record.status,
        date_of_establishment: iec_record.date_of_establishment
      } }
    end
  end
end
